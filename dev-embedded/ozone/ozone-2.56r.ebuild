# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit versionator unpacker eutils

# for Ozone_V222q style version
#MY_PV=$(delete_version_separator)
#MY_P="${MY_PN}_V${MY_PV}"
#MY_P_DL="${MY_PN}_Linux_V${MY_PV}"
# for 2.22.17 style version
read LIBV1 LIBV2 LIBV3 <<< $(get_version_components)
LIBV2=$((${LIBV2} + 0))
if [ ! -z ${LIBV3} ]; then
	LIBV3=$(($(printf '%d' "'${LIBV3}'") - 97 + 1))
	MY_PV="${LIBV1}.${LIBV2}.${LIBV3}"
else
	LIBV3="0"
	MY_PV="${LIBV1}.${LIBV2}" #.${LIBV3}"
fi

DESCRIPTION="The J-Link Debugger"
HOMEPAGE="https://www.segger.com/ozone.html"
SRC_URI="
	amd64? ( https://www.segger.com/downloads/jlink/${PN}_${MY_PV}_x86_64.deb )
	x86? ( https://www.segger.com/downloads/jlink/${PN}_${MY_PV}_i386.deb )
"

LICENSE="segger"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RESTRICT="mirror strip"

RDEPEND=""

QA_PREBUILT="*"

S=${WORKDIR}/

src_unpack() {
	unpack_deb ${A}
}

src_prepare() {
	default
	sed -i 's|\/usr\/bin|\/opt\/bin|' "usr/share/applications/segger-${PN}-${MY_PV}.desktop" || die "sed failed"
}

src_install() {
	insinto /opt
	doins -r opt/SEGGER

	chmod 0755 "${D}/opt/SEGGER/${PN}/${MY_PV}/Ozone"
	dosym "../SEGGER/${PN}/${MY_PV}/Ozone" "/opt/bin/Ozone"

	domenu "usr/share/applications/segger-${PN}-${MY_PV}.desktop"
}
