# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit versionator udev unpacker

MY_PV=$(delete_version_separator)
MY_PN="JLink"
MY_P="${MY_PN}_V${MY_PV}"
MY_P_DL="${MY_PN}_Linux_V${MY_PV}"
read LIBV1 LIBV2 LIBV3 <<< $(get_version_components)
LIBV2=$((${LIBV2} + 0))
if [ ! -z ${LIBV3} ]; then
	LIBV3=$(($(printf '%d' "'${LIBV3}'") - 97 + 1))
else
	LIBV3="0"
fi

DESCRIPTION="Segger GDB Server and J-Link Commander"
HOMEPAGE="http://www.segger.com/jlink-software.html"
SRC_URI="
	amd64? ( ${MY_P_DL}_x86_64.deb )
	x86? ( ${MY_P_DL}_i386.deb )
"

LICENSE="segger"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="examples"

RESTRICT="fetch strip"

RDEPEND=""

QA_PREBUILT="*"

S=${WORKDIR}/

JBINARIES="JFlashLiteExe
	JFlashSPI_CL
	JLinkExe
	JLinkGDBServer
	JLinkGDBServerCLExe
	JLinkGDBServerExe
	JLinkLicenseManager
	JLinkLicenseManagerExe
	JLinkRegistration
	JLinkRegistrationExe
	JLinkRemoteServer
	JLinkRemoteServerCLExe
	JLinkRemoteServerExe
	JLinkRTTClient
	JLinkRTTLogger
	JLinkSTM32
	JLinkSWOViewer
	JTAGLoadExe
"

pkg_nofetch() {
	einfo "Please download"
	if use amd64; then
		einfo " -> ${MY_P_DL}_x86_64.deb"
	else
		einfo " -> ${MY_P_DL}_i386.deb"
	fi
	einfo "from ${HOMEPAGE} and place it in ${DISTDIR}"
}

src_unpack() {
	unpack_deb ${A}
}

src_prepare() {
	rm -Rf "opt/SEGGER/${MY_P}/ThirdParty"
	if ! use examples; then
		rm -Rf "opt/SEGGER/${MY_P}/Samples"
	fi
	default
}

src_install() {
	insinto /opt
	doins -r opt/SEGGER

	for file in ${JBINARIES}; do
		chmod 0755 "${D}/opt/SEGGER/${MY_P}/${file}"
		dosym "../SEGGER/${MY_PN}/${file}" "/opt/bin/${file}"
	done

	udev_dorules etc/udev/rules.d/99-jlink.rules
}

pkg_postinst() {
	einfo "Following RTOS plugins for Segger's JlinkGDBServer have been installed into"
	einfo "'/opt/SEGGER/${MY_P}/GDBServer':"
	for p in $(ls /opt/SEGGER/${MY_P}/GDBServer/RTOSPlugin*.so); do
		einfo "  $(basename ${p})"
	done
	einfo "to use them, call JlinkGDBServer with '-rtos /opt/SEGGER/${MY_P}/GDBServer/PLUGIN.so'"
}
