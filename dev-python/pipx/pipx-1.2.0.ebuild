# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=hatchling
DISTUTILS_SINGLE_IMPL=1
PYTHON_COMPAT=( python3_{9..11} )
inherit distutils-r1

SRC_URI="https://github.com/pipxproject/${PN}/archive/${PV}.tar.gz"
KEYWORDS="~amd64"

DESCRIPTION="Install and Run Python Applications in Isolated Environments"
HOMEPAGE="https://pipxproject.github.io/pipx/"

LICENSE="MIT"
SLOT="0"
IUSE=""

RESTRICT="test"

RDEPEND="
	$(python_gen_cond_dep '
		dev-python/argcomplete[${PYTHON_USEDEP}]
		dev-python/packaging[${PYTHON_USEDEP}]
		dev-python/platformdirs[${PYTHON_USEDEP}]
		dev-python/userpath[${PYTHON_USEDEP}]
	')
"
