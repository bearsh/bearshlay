# Bearshlay #

As this overlay is currently not listed in layman's index, add it with the following command as root to the local list of overlays:


```
#!bash

wget -O /etc/layman/overlays/bearshlay.xml https://bitbucket.org/bearsh/bearshlay/raw/HEAD/Documentation/overlay.xml
```