# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="a diff-capable 'du-browser'"
HOMEPAGE="http://gt5.sourceforge.net/"
SRC_URI="http://deb.debian.org/debian/pool/main/g/${PN}/${PN}_1.5.0~20111220+bzr29.orig.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="|| ( www-client/links
		www-client/elinks
		www-client/lynx )"

PATCHES=(
	"${FILESDIR}/10-bug-758634.patch"
        "${FILESDIR}/20-bug-758645.patch"
)

src_unpack() {
	default
	mv gt5* ${P}
}

src_prepare() {
	default
	sed -i "s|^version=.*|version=${PV}|" gt5 || die "sed failed"
}
