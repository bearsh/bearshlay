# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils flag-o-matic user systemd

DESCRIPTION="Squeezelite is a small headless Squeezebox emulator for Linux using ALSA audio output"
HOMEPAGE="https://code.google.com/p/squeezelite"

SRC_URI="https://bitbucket.org/bearsh/squeezelite/get/v${PV}.tar.bz2 -> ${P}.tar.bz2"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~arm"
IUSE="flac vorbis mad mpg123 aac ffmpeg resample dsd"

DEPEND="media-libs/alsa-lib
	flac?     ( media-libs/flac )
	vorbis?   ( media-libs/libvorbis )
	mad?      ( media-libs/libmad )
	mpg123?   ( media-sound/mpg123 )
	aac?      ( media-libs/faad2 )
	ffmpeg?   ( media-video/ffmpeg )
	resample? ( media-libs/soxr )
"
RDEPEND="${DEPEND}
	 media-sound/alsa-utils
"

pkg_setup() {
	# Create the user and group if not already present
	enewuser squeezelite -1 -1 "/dev/null" audio
}

src_unpack() {
	default
	cd "${WORKDIR}"
	mv *-${PN}-* "${S}"
}

src_prepare () {
	# Apply patches
	epatch "${FILESDIR}/1.6-make-different-codes-optional.patch"
	epatch "${FILESDIR}/1.6-Makefile.patch"
}

src_compile() {
	# Configure optional codec support; this is added to the original
	# source via a patch in this ebuild at present.
	if use ffmpeg; then
		append-cflags "-DFFMPEG"
	fi
	if use dsd; then
		append-cflags "-DDSD"
	fi
	if use resample; then
		append-cflags "-DRESAMPLE"
	fi
	# disable normaly on options if requested
	if ! use flac; then
		append-cflags "-DNO_FLAC"
		einfo "FLAC support disabled; add 'flac' USE flag if you need it"
	fi
	if ! use vorbis; then
		append-cflags "-DNO_OGG"
		einfo "Ogg/Vorbis support disabled; add 'vorbis' USE flag if you need it"
	fi
	if ! use mad; then
		append-cflags "-DNO_MAD"
	fi
	if ! use mpg123; then
		append-cflags "-DNO_MPG123"
	fi
	if ! use mad && ! use mpg123; then
		einfo "MP3 support disabled; add 'mad' (recommended)"
		einfo "  or 'mpg123' USE flag if you need it"
	fi
	if ! use aac; then
		append-cflags "-DNO_AAC"
		einfo "AAC support disabled; add 'aac' USE flag if you need it"
	fi

	# link it during building rather than at runtime
	append-cflags "-DLINKALL"
	# build it
	emake || die "emake failed"
}

src_install() {
	dobin squeezelite
	dodoc LICENSE.txt

	newconfd "${FILESDIR}/${PN}.conf.d" "${PN}"
	newinitd "${FILESDIR}/${PN}.init.d" "${PN}"
	systemd_dounit "${FILESDIR}/${PN}.service"
}

pkg_postinst() {
	# Provide some post-installation tips.
	elog "If you want start Squeezelite automatically on system boot:"
	elog "  rc-update add squeezelite default #for openrc"
	elog "or"
	elog "  systemctl enable squeezelite #for systemd"
	elog "Edit /etc/cond.d/squeezelite to customise -- in particular"
	elog "you may want to set the audio device to be used."
}
