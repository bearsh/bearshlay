# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"
PYTHON_COMPAT=( python{3_3,3_4} ) 

inherit distutils-r1

if [[ "${PV}" == *pre* ]]; then
	MY_PV="860f89e60a56ed577ab25d60a35bf3c8a5bfe053"
else
	MY_PV="v${PV}"
fi

DESCRIPTION="An easy to use desktop tool to quickly create and destroy SSH tunnels and launch commands based on a preconfigured setup."
HOMEPAGE="http://github.com/mtorromeo/iosshy http://kde-apps.org/content/show.php/IOSSHy?content=119689"
SRC_URI="http://github.com/mtorromeo/iosshy/tarball/${MY_PV} -> ${P}.tgz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+kde"

RDEPEND="dev-python/paramiko[${PYTHON_USEDEP}]
	dev-python/keyring[${PYTHON_USEDEP}]
	dev-python/setproctitle[${PYTHON_USEDEP}]
	kde? ( kde-base/pykde4[${PYTHON_USEDEP}] )"
DEPEND="${RDEPEND}
	dev-python/PyQt4[${PYTHON_USEDEP}]"

DOCS="LICENSE"

# github archiv $S workaround
src_unpack() {
	unpack ${A}
	mv *-${PN}-* "${S}"
}

src_compile() {
	sh build.sh
	default
}
